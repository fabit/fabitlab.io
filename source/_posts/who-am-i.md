---
title: Who am i?
---
Fabio Pagnotta is an italian guy. He was born at 11th November 1992. He is a system enginner and software developer. In this field, he likes to follow and sponsor open source projects that are essential every day.

## Education

1. 2006 – 2011,  **ITIS G. Marconi** - Secondary School Diploma in Industrial Engineering Computer science,<b>*</b>
2. 2012 – 2015, **University of Camerino, Bachelor degree Informatica**,  **
3. 2015 – 2017, **University of Camerino, Master degree Computer science**, **
4. 2018 –– now, **University of Camerino, PhD ( Modelling and simulation of IoT system)**,
5. 2013 – 2019, **Member of Camerino Linux User Group (CAMELUG)**

<b>*</b> 77/110 - Jesi, Ancona, Italy
** 110/110 Lode, Camerino (MC), Italy
## Certifications

1. **Information’s Engineer Exam**, 2014
2. **CISCO CCNA 1 e 2**, 4 December 2017 

## Work experiences
1. 2016 – 2017, **E-Lios Srl**, System engineer, Camerino (MC), Italy

    • Fabio Pagnotta had been working with public administrations, in the company E-Lios Srl. His main task had been the distribution
    of new updates on the servers of the Italian municipalities managed by the Halley company.
2. 2018 – Now, **Easly**, CTO, System engineer, backend expert

    Fabio Pagnotta is part of a startup called Easly along with some of my friends. Easly is a marketplace created to meet the demand
    and demand for cleaning jobs in Portugal. In this platform you can book cleaning services for your room or for the
    whole house. We are currently exploring the various market opportunities in Portugal. In this scenario, I am the
    CTO, frontend / backend developer and responsible for maintaining the GNU / Linux instances and the backend.
3. 07-09/2021 **Mondo Novo Electronics Srl**, System engineer and backend/IoT developer Matelica (MC), Italy
    Collaboration with the Mondonovo company in the IoT field.

## Abilities

• Operating systems: Debian (7/10), Ubuntu (7/10), Linux mint (7/10), Archlinux (8/10), Manjaro (8/10), Raspbian
(7/10)
• Programming languages: C ( 7/10 ), C++ ( 7/10 ), Java ( 6/10 ), Python ( 8/10 ), Bash ( 8/10 ), Javascript (7.5/10)),
Typescript (7.5/10)), PHP (6/10), MongoDB ( 8/10)
• Pub/Sub: MQTT (9/10), PICO-MP (10)
• Formatting languages: CSS (7/10), SCSS ( 6/10), SASS (6/10)
• Markup languages: HTML 5 (8/10)
• Framework Frontend: Angular (7/10)
• Framework Backend: Nestjs (8/10)
• Structured languages: SQL (7/10)
• Languages: Italian (Native language), English

## Projects

• Unicam email migration SMTP, IMAP, POP
The University of Camerino had changed the mail system from a private system to a google mail service (or also called
Gmail). In this scenario, I was migrating the old accounts to the new system
• Tech4Care cloud migration to the SM3.0 portal (Mondo Novo Electronics Srl) Docker,
Bash, Debian 11, SSH, Continuos integration, Nginx, GIT
Migration of an Italian cloud platform for telemedicine from Azure cloud to Google cloud. Migration includes moving
frontend, backend and Jitsi services.
• CCHURE Project (UNICAM) MQTT, LoRaWAN, Certificati SSL, SSH, Chirpstack, GIT, Integrazione continua, Mysql, PICO-MP (https://sites.google.com/unicam.it/cchure/home)
The CCHURE project proposes to define a transdisciplinary methodology to evaluate the effects that climate change
produces on ”urban health”. My contribution was the creation of a Chirpstack infrastructure in an Ubuntu instance for
data acquisition and monitoring through low - power protocols (such as LoRaWAN).
• 4Helix Project (UNICAM) Bash, PHP, Mysql, GIT, SSH, Apache
https://4helix.unicam.it/Deployment and management of 4helix services ( which includes also continuous integration )
• Coastenergy Project (UNICAM) Bash, PHP, Mysql, GIT, SSH, Apache https://coastenergy.unicam.it
Deployment and management of Coastenergy services ( which includes also continuous integration )
• Easly website Git, Debian, SSH, Integrazione continua, NestJS, Typescript https://easly.space/
Management and maintenance of the easly.space production and test marketplace. Frontend development and in
particular backend of the platform respectively in Angular and in Nodejs.
• Bike sharing (Mondo Novo Electronics Srl) Git, Debian, SSH, Integrazione continua, NestJS, MQTT,Typescript
Management and development of a parking sharing platform deployed on a Debian server. Backend development of the platform in typescript using the Nodejs runtime.
• Thingsboard Community (Mondo Novo Electronics Srl) Thingsboard, Ubuntu https://thingsboard.io/
Learning and deployment of an open source platform for the collection, processing and visualization of data from IoT devices.
• Perfume sprayers system at GITEX World Trade Center, Dubai 2020 (Mondo Novo
Electronics Srl) Python 3, Modbus, Flask, Concorrenza, Raspberry pi
Development of a python application for the management of MODBUS slaves. Each slave is associated with several devices (such as sprinklers, pumps and reserves).
• Mercury Project( UNICAM - Filippetti ) Buildroot, C, GNU/Linux, Edge computing, Bash https://buildroot.org/
Design and development of a data distribution model for collaborative cyber-physical systems. In this project, I have
been involved in bringing a framework written in c from a desktop architecture to a 32-bit armv7l architecture on a GNU / Linux system created through Buildroot.
• Medical screening device, Endomax Python 3, Kivy, Raspberry pi, Serial, Raspbian
Development and deployment of an application with a graphical interface for a medical device
• Software developer Playstation 1 C, Playstation 1, Playstation 2
Development of a benchmark application between PS1 and PS2 (backward compatible with PS1) in C language.
Graphical and computational performances were compared in the study
Treasure hunt with QR code developer HTML, CSS, Javascript, Bootstrap, PHP, Mysql
Development of a treasure hunt game with the Camelug association (PHP, HTML, CSS, Javascript, Bootstrap). The aim was to promote the territory and points of interest of the city of Camerino.
• PICO-MP: A publisher / subscriber multi-architecture middleware for sensors C++, HWSAN
Development of a pub / sub middleware for the master’s and doctoral thesis. The middleware verifies in real time a
variant of the first order logic formula in the context of the HWSAN. This is done by efficiently distributing the
formulas across the network.
• Electromagnetic Field (EMF) Pollution Detector Wiring (Arduino uno), Zigbee, PHP, JS, CSS
Creation of an electromagnetic field detector for a university project. Nowadays, EF pollution is part of our life. This is particularly evident in large cities. The aim of this project was to measure the electromagnetic pollution given by
smartphones.
• Data mining Knime, https://shorturl.at/nozC8
Alcohol addiction is a real problem in our society. In this article, we delve into this problem by trying to identify those
at risk among students.

## Teaching
2015-2016: Designing and Managing Databases Tutor (UNICAM) 
2018-2019: Cyberchallenge instructor (UNICAM)  (https://cyberchallenge.it/)
2018-2019: Operating systems Tutor (UNICAM) 
2019: Computer architecture Tutor (UNICAM) 
2020: MQTT 3.1 and MQTT 5 Lesson (UNICAM) 
2020: Prototyping IoT Lesson (UNICAM) 
2021: Angular instructor (Mondo Novo Electronics Srl) 